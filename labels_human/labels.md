﻿## Human-made labels for FPI DIS

### November 2017

`labels_fpi_fast_dis_dist_201711.cdf`  
Total number of examples in the dataset  278110

| # | Class         | Number of samples |
|---|---------------|---------------|
|-1 | Unknown       |  41685 (15.0%) |
| 0 | Solar Wind    | 119934 (43.1%) |
| 1 | Foreshock     |  24775 (8.9%)  |
| 2 | Magnetosheath |  67085 (24.1%) |
| 3 | Magnetosphere |  24631 (8.9%)  |

### December 2017

`labels_fpi_fast_dis_dist_201712.cdf`  
Total number of examples in the dataset  191168

| # | Class         | Number of samples |
|---|---------------|---------------|
|-1 | Unknown       | 28332 (14.8%) |
| 0 | Solar Wind    | 69719 (36.5%) |
| 1 | Foreshock     | 20455 (10.7%) |
| 2 | Magnetosheath | 51312 (26.8%) |
| 3 | Magnetosphere | 21350 (11.2%) |
