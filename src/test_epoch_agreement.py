"""Check whether epochs in human-made and model-made labels disagree.

"""
from labeller import *
import argparse

if __name__ == "__main__":
    parser = argparse.ArgumentParser('Test if epochs in human-labelled \
        dataset agree with those in model-labeled')
    parser.add_argument('model_labels_cdf', help='Path to the CDF file with model-made labels')
    parser.add_argument('fpi_cdf', help=r'Path to the FPI DIST CDF file, \
        e.g., mms/mms1/fpi/fast/l2/dis-dist/2017/11/mms1_fpi_fast_l2_dis-dist_20171103220000_v3.3.0.cdf')

    args = parser.parse_args()

    eslice = slice(33922, 33925)

    # 1. Human-made labels
    lbl_dict = mu.read_labels_cdf(r'../labels_human/labels_fpi_fast_dis_dist_201711.cdf')
    epoch_human = lbl_dict['epoch']

    # 2. Model-made labels
    model_dict = mu.read_labels_cdf(args.model_labels_cdf)
    epoch_model = model_dict['epoch']

    # 3. Labels from MMS1 FPI CDF file
    fpi_dict = mu.read_fpi_cdf(args.fpi_cdf)
    epoch_fpi = fpi_dict['epoch']

    # Printing
    print('\n\n')
    print(f'Epochs from CDF with human-made labels {epoch_human[eslice]}')
    print(f'Times for these epochs {[mu.epoch2time(e) for e in epoch_human[eslice]]}')

    print(f'Epochs from CDF with model-made labels {epoch_model[eslice]}')
    print(f'Times for these epochs {[mu.epoch2time(e) for e in epoch_model[eslice]]}')

    print(f'Number of entries in the FPI CDF file: {len(epoch_fpi)}')
    # Epoch 563025663699921000
    i = np.where(epoch_fpi == epoch_human[eslice.start])[0]
    print(f'Index of the entry with epoch={epoch_human[eslice.start]}: {i}')
