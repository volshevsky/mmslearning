# Routines for automatic classification and understanding of MMS observations**

Project has started between KTH and IRFU in spring of 2019.

## Installation

1. Install Anaconda + TensorFlow 2
2. `git clone https://bitbucket.org/volshevsky/mmslearning/`
3. `pip install -U cdflib scikit-learn`

## Read human-labelled data

Human-made labels are saved in CDF files. One file corresponds to 1 month of data.
| Label | Abbreviation | Description |
|-------|--------------|-------------|
|  -1   |     UNK      | Unknown/unclassified |
|   0   |      SW      | Solar wind |
|   1   |      IF      | Ion Foreshock |
|   2   |     MSH      | Magnetosheath |
|   3   |     MSP      | Magnetosphere |

To read and plot the labels, use `classifier.py`. Uncomment the lines
```
    lbl_dict = mu.read_labels_cdf(r'../labels_human/labels_fpi_fast_dis_dist_201711.cdf')
    demo_labels(lbl_path, date, lbl_dict=lbl_dict)
```

## Contact

*Vyacheslav Olshevsky* sya.olshevsky@gmail.com
